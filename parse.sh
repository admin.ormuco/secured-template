#!/bin/sh
# $1 PROJECT_NAME
# $2 DOCKER_REGISTRY_URL
# $3 SONARQUBE_URL

sed -e "s/\${PROJECT_NAME}/$1/" -e "s/\${DOCKER_REGISTRY_URL}/$2/" deployment.yaml > deployment.yaml.tmp && mv deployment.yaml.tmp deployment.yaml
sed -e "s/\${PROJECT_NAME}/$1/" -e "s/\${SONARQUBE_URL}/$3/" sonar-project.properties > sonar-project.properties.tmp && mv sonar-project.properties.tmp sonar-project.properties
sed -e "s/\${PROJECT_NAME}/$1/" service.yaml > service.yaml.tmp && mv service.yaml.tmp service.yaml

